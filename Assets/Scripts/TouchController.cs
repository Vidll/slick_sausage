using UnityEngine;

public class TouchController : MonoBehaviour
{
    [SerializeField] private Frankfuter _frankfuter;

    private Vector2 _startPos;
    private Vector2 _direction;

    private bool _touchReady = true;

    private void Start()
    {
        _frankfuter = GetComponent<Frankfuter>();
    }

    private void Update()
    {
        if (Input.touchCount > 0 && _frankfuter.OnGround && _touchReady)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startPos = touch.position;
                    break;

                case TouchPhase.Moved:
                    _direction = touch.position - _startPos;
                    _frankfuter.ShowTrajectory(-_direction);
                    break;
                    
                case TouchPhase.Ended:
                    _frankfuter.ResetTrajectoryPoints();
                    _frankfuter.ThrowObject(_direction);
                    break;
            }
        }
        else if (Input.touchCount > 0)
		{
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _touchReady = false;
                    break;

                case TouchPhase.Ended:
                    _touchReady = true;
                    break;
            }
        }
    }
}
