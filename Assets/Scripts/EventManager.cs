using UnityEngine;

public class EventManager : MonoBehaviour
{
    public delegate void ActionOnGame();

    public ActionOnGame FrankfuterDieAction;
    public ActionOnGame StartGameAction;
    public ActionOnGame FinishOnGame;
}
