using UnityEngine;

public class CameraFollowing : MonoBehaviour
{
	private const string NAME_TAG_CUSTOM_EVENT_MANAGER = "EventManager";
	private const string NAME_TAG_PLAYER = "Player";

	[SerializeField] private Transform _followObject;
	[SerializeField] private EventManager _eventManager;

	private Vector3 _cameraPosition;

	private void Start()
	{
		_eventManager = GameObject.FindGameObjectWithTag(NAME_TAG_CUSTOM_EVENT_MANAGER)
			.GetComponent<EventManager>();
		_cameraPosition = this.gameObject.transform.position;

		_eventManager.StartGameAction += SetFollowObject;
	}

	private void Update()
	{
		if(_followObject != null)
			this.gameObject.transform.position = _followObject.transform.position + _cameraPosition;
	}

	private void SetFollowObject()
	{
		_followObject = GameObject.FindGameObjectWithTag(NAME_TAG_PLAYER).GetComponent<Transform>();
	}
}