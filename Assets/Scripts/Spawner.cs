using UnityEngine;

public class Spawner : MonoBehaviour
{
	private const string NAME_TAG_CUSTOM_EVENT_MANAGER = "EventManager";

	[SerializeField] private Frankfuter _player;
	[SerializeField] private EventManager _eventManager;

	private void Start()
	{
		_eventManager = GameObject.FindGameObjectWithTag(NAME_TAG_CUSTOM_EVENT_MANAGER)
			.GetComponent<EventManager>();

		_eventManager.StartGameAction += SpawnPlayer;
	}

	private void SpawnPlayer()
	{
		_eventManager.StartGameAction -= SpawnPlayer;
		var player = Instantiate(_player);
		player.transform.parent = this.transform;
	}
}
