using UnityEngine;

public class MapCreator : MonoBehaviour
{
	private const string NAME_TAG_CUSTOM_EVENT_MANAGER = "EventManager";

	[SerializeField] private GameObject[] _cubePrefabs;
	[SerializeField] private GameObject _finishCube;
	[SerializeField] private EventManager _eventManager;

	[SerializeField] private int _levelLength;
	[SerializeField] private int _spaceBetweenCubes;

	private float _movedNumber = 0;

	private void Start()
	{
		_eventManager = GameObject.FindGameObjectWithTag(NAME_TAG_CUSTOM_EVENT_MANAGER)
			.GetComponent<EventManager>();

		_eventManager.StartGameAction += CreateObjects;
	}

	private void CreateObjects()
	{
		_eventManager.StartGameAction -= CreateObjects;

		for (int i = 0; i < _levelLength; i++)
		{
			Create(_cubePrefabs[GetRandomNumber(0, _cubePrefabs.Length)]);
		}

		Create(_finishCube);
	}

	private void Create(GameObject createObject)
	{
		if (createObject != null)
		{
			var obj = Instantiate(createObject);
			obj.transform.parent = this.transform;

			obj.transform.localScale = new Vector3
				(GetRandomFloat(2, 5), GetRandomFloat(5, 13), obj.transform.localScale.z);
			obj.transform.position = new Vector3
				(obj.transform.position.x + _movedNumber, obj.transform.position.y, obj.transform.position.z);
			obj.transform.rotation = Quaternion.Euler
				(obj.transform.rotation.x, obj.transform.rotation.y , GetRandomFloat(-8f, 8f));
			_movedNumber += obj.transform.localScale.x;
		}
		else
		{
			_movedNumber += _spaceBetweenCubes;
		}
	}

	private float GetRandomFloat(float min, float max) => Random.RandomRange(min, max);
	private int GetRandomNumber(int min, int max) => Random.Range(min, max);
}