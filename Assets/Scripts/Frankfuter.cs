using UnityEngine;

public class Frankfuter : MonoBehaviour
{
	private const string NAME_TAG_GROUND = "Ground";
	private const string NAME_TAG_CUSTOM_EVENT_MANAGER = "EventManager";
	private const string NAME_TAG_FINISH = "Finish";

	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private LineRenderer _lineRenderer;
	[SerializeField] private EventManager _eventManager;
	
	[SerializeField] private float _lowerTresholdForY;
	[SerializeField] private float _maxPoverThrow;

	private Vector3[] _trajectoryPoints = new Vector3[5];

	public bool OnGround = true;

	private void Start()
	{
        _rigidbody = GetComponent<Rigidbody>();
		_lineRenderer = GetComponent<LineRenderer>();
		_eventManager = GameObject.FindGameObjectWithTag(NAME_TAG_CUSTOM_EVENT_MANAGER)
			.GetComponent<EventManager>();
	}

	private void Update()
	{
		if (this.gameObject.transform.position.y < _lowerTresholdForY)
			DestroyFrankfuter();
		else if (_rigidbody.velocity == Vector3.zero)
			OnGround = true;
	}

	public void ThrowObject(Vector2 direction)
	{
		if (direction.y < -_maxPoverThrow)
			direction.y = -_maxPoverThrow;

		_rigidbody.AddForce(Vector3.up * (-direction.y), ForceMode.Impulse);
		_rigidbody.AddForce(Vector3.left * direction.x, ForceMode.Impulse);
	}

	public void ShowTrajectory(Vector3 direction)
	{
		if (OnGround)
		{
			_lineRenderer.positionCount = _trajectoryPoints.Length;

			for (int i = 0; i < _trajectoryPoints.Length; i++)
			{
				var time = i * 0.1f;
				_trajectoryPoints[i] = transform.position +
					new Vector3(direction.x / 30, direction.y / 30, direction.z) *
						time + Physics.gravity * time * time / 2;
			}
			_lineRenderer.SetPositions(_trajectoryPoints);
		}
	}

	private void DestroyFrankfuter()
	{
		Destroy(this.gameObject);
		_eventManager?.FrankfuterDieAction.Invoke();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag(NAME_TAG_GROUND))
			OnGround = true;
		else if (other.gameObject.CompareTag(NAME_TAG_FINISH))
			_eventManager?.FinishOnGame.Invoke();
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag(NAME_TAG_GROUND))
			OnGround = false;
	}

	public void ResetTrajectoryPoints() => _lineRenderer.positionCount = 0;
}