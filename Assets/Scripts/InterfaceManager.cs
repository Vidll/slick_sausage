using UnityEngine;
using UnityEngine.SceneManagement;

public class InterfaceManager : MonoBehaviour
{
	private const string NAME_TAG_CUSTOM_EVENT_MANAGER = "EventManager";
	private const string NAME_MAIN_SCEEN = "Main";

	[SerializeField] private GameObject _startWindow;
	[SerializeField] private GameObject _failWindow;
	[SerializeField] private GameObject _winWindow;
	[SerializeField] private EventManager _eventManager;

	private bool _finishGame = false;

	private void Start()
	{
		_eventManager = GameObject.FindGameObjectWithTag(NAME_TAG_CUSTOM_EVENT_MANAGER)
			.GetComponent<EventManager>();

		_eventManager.FrankfuterDieAction += GameOver;
		_eventManager.FinishOnGame += FinishGame;

		_startWindow.SetActive(true);
	}

	private void GameOver()
	{
		_eventManager.FrankfuterDieAction -= GameOver;
		if (!_finishGame)
			_failWindow.SetActive(true);
	}
	private void FinishGame()
	{
		_eventManager.FinishOnGame -= FinishGame;
		_finishGame = true;
		_winWindow.SetActive(true);
	}

	public void Restart()
	{
		SceneManager.LoadScene(NAME_MAIN_SCEEN);
	}

	public void StartGame()
	{
		_eventManager?.StartGameAction.Invoke();
		_startWindow.SetActive(false);
	}
}
